import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './component/app.component';
import { ComponenteApp1Component } from './componente-app1/componente-app1.component';
import { ComponenteApp2Component } from './componente-app2/componente-app2.component';
import { ShareModule } from './modulos/modulo-a/share.module';
import { ShareA1Component } from './modulos/modulo-a/share-a1/share-a1.component';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [
    AppComponent,
    ComponenteApp1Component,
    ComponenteApp2Component,
  ],
  imports: [
    BrowserModule,

    ShareModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
