import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  currentNumber = 1;
  showApp1 = true;

  nombreEnApp = 'Pedro';

  onClick(): void {
    console.log('onClick');
    this.currentNumber++;
  }

  updateName(): void {
    this.nombreEnApp = 'Ivan';
  }

  appEvent(nombre: string) {
    console.log('Compoenent App Event!!', nombre);
  }
}
