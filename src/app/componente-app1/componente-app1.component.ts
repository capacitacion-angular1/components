import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-componente-app1',
  templateUrl: './componente-app1.component.html',
  styleUrls: ['./componente-app1.component.css']
})
export class ComponenteApp1Component implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('input') input: ElementRef;

  inputValue = 'Hol';

  constructor() {
    console.log('constructor');
    this.inputValue = 'Hola mundo';

    console.log('constructor input', this.input);
  }

  ngOnDestroy(): void {
    console.log('ngOnDestroy');
  }

  ngOnInit(): void {
    console.log('ngOnInit');
  }

  ngAfterViewInit(): void {
    console.log('ngAfterViewInit');
    console.log('ngAfterViewInit input', this.input);
    this.input.nativeElement.value = 'ngAfterViewInit';
  }



}
