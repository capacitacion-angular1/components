import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-componente-app2',
  templateUrl: './componente-app2.component.html',
  styleUrls: ['./componente-app2.component.css']
})
export class ComponenteApp2Component implements OnInit, OnChanges {
  @Input() nombreActual;
  @Output() newEvent = new EventEmitter<string>();

  constructor() {
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('changes', changes);
  }

  // tslint:disable-next-line:typedef
  emmitEvent(currentInputData){
    this.newEvent.emit(currentInputData);
  }

}
