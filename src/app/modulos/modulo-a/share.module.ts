import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShareA1Component } from './share-a1/share-a1.component';
import { ShareA2Component } from './share-a2/share-a2.component';

@NgModule({
  declarations: [
    ShareA1Component,
    ShareA2Component
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ShareA1Component,
    ShareA2Component
  ],
})
export class ShareModule {
}
